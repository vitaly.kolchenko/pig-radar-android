plugins {
    id("android-library-convention")
    id("kotlin-kapt-convention")
    id("androidx.navigation.safeargs.kotlin")
}

android {
    defaultConfig {
        namespace = "net.pigradar.map"
    }
    buildFeatures {
        viewBinding = true
    }

    testOptions {
        unitTests {
            isIncludeAndroidResources = true
        }
    }
}



dependencies {
    implementation(projects.core)
    api(projects.pigData)

    implementation(libs.bundles.androidx)

    implementation(libs.osm)

    implementation(libs.androidx.room.runtime)

    testImplementation(libs.androidx.room.testing)

    testImplementation(libs.bundles.unitTest)

    testImplementation(libs.kotlinReflection)

    kaptTest(libs.hiltCompiler)

}
