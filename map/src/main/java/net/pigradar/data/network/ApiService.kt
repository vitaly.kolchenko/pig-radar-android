package net.pigradar.data.network

import net.pigradar.data.CreatePointReq
import net.pigradar.data.Point


internal interface ApiService {
    suspend fun addPoint(req: CreatePointReq): Point

    suspend fun getPoints(lat: Double, lng: Double, radius: Int, byHours: Int): List<Point>

    suspend fun updatePoint(point: Point)

    suspend fun removePoint(pointId: Int)
}