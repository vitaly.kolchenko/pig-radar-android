package net.pigradar.data.network

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import net.pigradar.data.CreatePointReq
import net.pigradar.data.Point
import net.pigradar.map.BuildConfig
import javax.inject.Inject

private val BASE_URL = "http://${BuildConfig.baseUrl}:8080"
private val POINT = "$BASE_URL/points"

internal class ApiServiceImpl @Inject constructor(private val httpClient: HttpClient) : ApiService {

    override suspend fun addPoint(req: CreatePointReq): Point {
        return httpClient.post(POINT) { setBody(req) }.body()
    }

    override suspend fun updatePoint(point: Point) {
        httpClient.put(POINT) { setBody(point) }
    }

    override suspend fun getPoints(lat: Double, lng: Double, radius: Int, byHours: Int): List<Point> {
        return httpClient.get(POINT) {
            parameter("lat", lat)
            parameter("lng", lng)
            parameter("radius", radius)
            parameter("byLastHours", byHours)
        }.body()
    }

    override suspend fun removePoint(pointId: Int) {
        httpClient.delete("$POINT/$pointId")
    }
}