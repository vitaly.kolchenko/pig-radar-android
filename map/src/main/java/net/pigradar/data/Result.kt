package net.pigradar.data

internal data class Result<T>(val source: Source, val data: T) {
    enum class Source { DB, NETWORK }
}