package net.pigradar.data

import net.pigradar.ui.PointUIModel

internal interface PointsRepo {
    suspend fun addPoint(req: CreatePointReq): Point

    suspend fun getPoints(lat: Double, lng: Double, radius: Int, byHours: Int): Result<List<PointUIModel>>

    suspend fun updatePoint(point: Point)

    suspend fun deletePoint(pointId: Int)
}