package net.pigradar.data

import io.ktor.utils.io.errors.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import net.pigradar.LocalStore
import net.pigradar.data.db.LocalPoint
import net.pigradar.data.db.LocalPointDao
import net.pigradar.data.network.ApiService
import net.pigradar.ui.PointUIModel
import net.pigradar.ui.toUiModel
import net.pigradar.userId
import timber.log.Timber
import javax.inject.Inject

internal class PointsRepoImpl @Inject constructor(
    private val apiService: ApiService,
    private val dao: LocalPointDao,
    private val localStore: LocalStore,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : PointsRepo {

    private val userId by lazy { localStore.userId() }

    override suspend fun addPoint(req: CreatePointReq): Point {
        val p = apiService.addPoint(req)
        dao.insertAll(listOf(p.toLocal()))
        return p
    }

    override suspend fun getPoints(lat: Double, lng: Double, radius: Int, byHours: Int): Result<List<PointUIModel>> =
        withContext(dispatcher) {
            try {
                val ps = apiService.getPoints(lat, lng, radius, byHours)
                dao.insertAll(ps.map(Point::toLocal))
                Result(Result.Source.NETWORK, ps.map { it.run { toUiModel(isMyPig(this)) } })
            } catch (e: IOException) {
                Timber.e(e)
                Result(Result.Source.DB, dao.getAll().map { it.toPoint().run { toUiModel(isMyPig(this)) } })
            }
        }

    override suspend fun updatePoint(point: Point) = withContext(dispatcher) {
        apiService.updatePoint(point)
        dao.update(point.toLocal())
    }

    override suspend fun deletePoint(pointId: Int) = withContext(dispatcher) {
        apiService.removePoint(pointId)
        dao.delete(pointId)
    }

    private fun isMyPig(pig: Point): Boolean {
        return userId == pig.owner
    }
}

internal fun Point.toLocal(): LocalPoint =
    LocalPoint(id, title, pigType.name, comment, upvotes, donwvotes, owner, location.lat, location.lng, creationDate)

internal  fun LocalPoint.toPoint(): Point =
    Point(id, title, PigType.valueOf(pigType), comment, upvotes, donwvotes, owner, Location(lat, lng), creationDate)

