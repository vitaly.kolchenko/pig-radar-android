package net.pigradar.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

@Dao
interface LocalPointDao {

    @Query("SELECT * FROM points")
    suspend fun getAll(): List<LocalPoint>

    @Query("SELECT * FROM points WHERE id IN (:ids)")
    suspend fun loadAllByIds(ids: IntArray): List<LocalPoint>

    @Insert (onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(points: List<LocalPoint>)

    @Update
    suspend fun update(point: LocalPoint)

    @Query("DELETE FROM points where id = :pointId")
    suspend fun delete(pointId: Int)
}