package net.pigradar.data.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import java.util.*

@Entity(tableName = "points")
data class LocalPoint(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val title: String,
    val pigType: String,
    val comment: String?,
    val upvotes: Int = 0,
    val donwvotes: Int = 0,
    val owner: Int,
    val lat: Double,
    val lng: Double,
    val creationDate: Date
)

class LPConverters {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time?.toLong()
    }
}