package net.pigradar.di

import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import net.pigradar.data.PointsRepo
import net.pigradar.data.PointsRepoImpl
import net.pigradar.data.network.ApiService
import net.pigradar.data.network.ApiServiceImpl

@Module
@InstallIn(SingletonComponent::class)
internal abstract class RepoModule {

    companion object{
        @Provides
        fun dispatcher(): CoroutineDispatcher = Dispatchers.IO
    }

    @Binds
    abstract fun bindApi(
        apiServiceImpl: ApiServiceImpl
    ): ApiService

    @Binds
    abstract fun funBindRepo(
        repoImpl: PointsRepoImpl
    ): PointsRepo
}