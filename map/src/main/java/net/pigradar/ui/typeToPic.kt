package net.pigradar.ui

import net.pigradar.data.PigType
import net.pigradar.map.R

internal fun typeToPic(pigType: PigType) = when (pigType) {
    PigType.COP_PIG -> R.drawable.police_pig
    PigType.WAR_PIG -> R.drawable.armor_pig
    PigType.UNDERCOVER_PIG -> R.drawable.glass_pig
    PigType.ROAD_PIG -> R.drawable.bird_pig
}