package net.pigradar.ui

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import net.pigradar.AuthStateDelegate
import net.pigradar.data.Point
import net.pigradar.data.PointsRepo
import net.pigradar.data.Result
import net.pigradar.onAFailure
import net.pigradar.runACatching
import org.orbitmvi.orbit.ContainerHost
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.postSideEffect
import org.orbitmvi.orbit.syntax.simple.reduce
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject

@HiltViewModel
internal class MapViewModel @Inject constructor(
    private val pointsRepo: PointsRepo,
    private val auth: AuthStateDelegate
) : ViewModel(), AuthStateDelegate by auth, ContainerHost<MapState, MapSideEffect> {

    override val container = container<MapState, MapSideEffect>(MapState(emptyList()))

    fun load(centerLat: Double, centerLng: Double, radius: Int, byPeriod: ByPeriod) = intent {
        runACatching {

            val byHours = when (byPeriod) {
                ByPeriod.HOUR -> 1
                ByPeriod.DAY -> 24
                ByPeriod.WEEK -> 24 * 7
            }

            val points = pointsRepo.getPoints(centerLat, centerLng, radius, byHours)
            reduce {
                state.copy(data = state.data + points.data)
            }
            if (points.source == Result.Source.DB) {
                postSideEffect(MapSideEffect.LoadedFromDB)
            }

        }.onAFailure {
            postSideEffect(MapSideEffect.LoadPointsError(it))
        }
    }

    fun update(point: Point) = intent {
        runACatching {
            pointsRepo.updatePoint(point)
            reduce {
                state.copy(data = state.data.map {
                    if (it.point.id == point.id) PointUIModel(
                        point,
                        it.info,
                        it.isMyPoint
                    ) else it
                })
            }
        }.onAFailure {
            postSideEffect(MapSideEffect.UpdatePointError(it))
        }
    }

    fun delete(pointId: Int) = intent {
        runACatching {
            pointsRepo.deletePoint(pointId)
            reduce { state.copy(data = state.data.filter { it.point.id != pointId }) }
        }.onAFailure {
            postSideEffect(MapSideEffect.DeletePointError(it))
        }
    }
}

internal enum class ByPeriod {
    HOUR, DAY, WEEK
}

internal data class MapState(val data: List<PointUIModel>)

internal sealed class MapSideEffect {
    object LoadedFromDB : MapSideEffect()

    data class AddPointError(val e: Throwable) : MapSideEffect()

    data class UpdatePointError(val e: Throwable) : MapSideEffect()

    data class DeletePointError(val e: Throwable) : MapSideEffect()

    data class LoadPointsError(val e: Throwable) : MapSideEffect()

    data class DefaultError(val e: Throwable) : MapSideEffect()
}

