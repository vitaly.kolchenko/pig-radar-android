package net.pigradar.ui

import net.pigradar.data.Point
import java.text.SimpleDateFormat
import java.util.*

internal data class PointUIModel(val point: Point, val info: String, val isMyPoint: Boolean) : java.io.Serializable

internal fun Point.toUiModel(isMyPig: Boolean = false) = PointUIModel(this, createPigInfo(this), isMyPig)

private fun createPigInfo(pig: Point): String {
    val df = SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault())
    val sb = StringBuilder(pig.title)
    pig.comment?.let { sb.append("\n", it) }
    sb.append("\n", df.format(pig.creationDate))
    return sb.toString()
}