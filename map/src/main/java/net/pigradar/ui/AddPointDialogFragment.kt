package net.pigradar.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint
import net.pigradar.data.PigType
import net.pigradar.map.R
import net.pigradar.map.databinding.FragmentAddPointDialogBinding
import net.pigradar.map.databinding.FragmentAddPointDialogItemBinding
import net.pigradar.observeAuthRequest
import org.orbitmvi.orbit.viewmodel.observe
import java.io.Serializable

@AndroidEntryPoint
internal class AddPointDialogFragment : BottomSheetDialogFragment() {

    private val viewModel: AddPointViewModel by viewModels()

    private var _binding: FragmentAddPointDialogBinding? = null
    private val binding get() = _binding!!

    private var savedStateHandle: SavedStateHandle? = null

    private val args by navArgs<AddPointDialogFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentAddPointDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.list.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        val itemAdapter = ItemAdapter()
        binding.list.adapter = itemAdapter

        savedStateHandle = findNavController().previousBackStackEntry?.savedStateHandle

        viewModel.observe(this, state = ::render, sideEffect = ::handleSideEffect)
        viewModel.observeAuthRequest(this)

        binding.done.setOnClickListener {
            viewModel.add(
                args.params.lat,
                args.params.lng,
                itemAdapter.getSelection(),
                binding.title.text.toString(),
                binding.description.text.toString()
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private inner class ViewHolder constructor(binding: FragmentAddPointDialogItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        val parent: View = binding.parent
        val image: ImageView = binding.image

        private var currentItem: PigType? = null

        fun bind(pigType: PigType) {
            if (currentItem != pigType) {
                image.setImageResource(typeToPic(pigType))
                currentItem = pigType
            }
        }
    }

    private inner class ItemAdapter : RecyclerView.Adapter<ViewHolder>() {
        private val items = PigType.values()
        private var previousSelection = 0
        private var selectedPosition = 0

        fun getSelection() = items[selectedPosition]

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val binding = FragmentAddPointDialogItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
            val vh = ViewHolder(binding)

            binding.parent.setOnClickListener {
                previousSelection = selectedPosition
                selectedPosition = vh.absoluteAdapterPosition

                notifyItemChanged(previousSelection)
                notifyItemChanged(selectedPosition)
            }

            return vh
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bind(items[position])
            holder.parent.isActivated = position == selectedPosition
        }

        override fun getItemCount(): Int {
            return items.size
        }
    }

    private fun render(state: AddPointState) {
        when (state) {
            AddPointState.Idle -> {
                binding.done.isEnabled = true
            }
            AddPointState.Loading -> {
                binding.done.isEnabled = false
            }
            is AddPointState.Success -> {
                binding.done.isEnabled = true
                savedStateHandle?.set("point", state.point)
                findNavController().popBackStack()
            }
        }
    }

    private fun handleSideEffect(sideEffect: AddPointSideEffect) {
        Toast.makeText(requireContext(), R.string.something_went_wrong, Toast.LENGTH_LONG)
            .show()
    }
}

data class Params(val lat: Double, val lng: Double) : Serializable
