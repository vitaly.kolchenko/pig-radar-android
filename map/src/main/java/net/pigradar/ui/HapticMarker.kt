package net.pigradar.ui

import android.view.HapticFeedbackConstants
import android.view.MotionEvent
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker

internal class HapticMarker(mapView: MapView?) : Marker(mapView) {

    override fun onLongPress(event: MotionEvent?, mapView: MapView?): Boolean {
        val result = super.onLongPress(event, mapView)
        if (result && (relatedObject as? PointUIModel)?.isMyPoint == true){
            mapView?.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)
            return true
        }
        return result
    }
}