package net.pigradar.ui

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.MarginLayoutParams
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isVisible
import androidx.core.view.updateMargins
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.transition.Slide
import androidx.transition.Transition
import androidx.transition.TransitionManager
import dagger.hilt.android.AndroidEntryPoint
import net.pigradar.PigConfig
import net.pigradar.data.Location
import net.pigradar.map.R
import net.pigradar.map.databinding.FragmentMapBinding
import net.pigradar.observeAuthRequest
import org.orbitmvi.orbit.viewmodel.observe
import org.osmdroid.api.IGeoPoint
import org.osmdroid.config.Configuration
import org.osmdroid.events.DelayedMapListener
import org.osmdroid.events.MapEventsReceiver
import org.osmdroid.events.MapListener
import org.osmdroid.events.ScrollEvent
import org.osmdroid.events.ZoomEvent
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.CustomZoomButtonsController
import org.osmdroid.views.overlay.MapEventsOverlay
import org.osmdroid.views.overlay.Marker
import javax.inject.Inject
import kotlin.math.roundToInt


@AndroidEntryPoint
class MapFragment : Fragment() {

    private val viewModel: MapViewModel by viewModels()

    @Inject
    lateinit var appConfig: PigConfig

    private var _binding: FragmentMapBinding? = null
    private val binding: FragmentMapBinding
        get() = _binding!!

    private lateinit var kebabTransition: Transition

    private var updateCandidateModel: Pair<PointUIModel, Marker>? = null
    private val markersMap = mutableMapOf<Int, HapticMarker>()
    private var byPeriodSelection: ByPeriod? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        Configuration.getInstance().userAgentValue = appConfig.appId
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMapBinding.inflate(layoutInflater)

        applyInsets()

        return binding.root
    }

    private fun applyInsets() {
        ViewCompat.setOnApplyWindowInsetsListener(binding.root) { _, insets ->
            (binding.fab.layoutParams as MarginLayoutParams).updateMargins(
                bottom = insets.getInsets(
                    WindowInsetsCompat.Type.navigationBars()
                ).bottom + resources.getDimensionPixelSize(R.dimen.fab_margin),
                right = insets.getInsets(
                    WindowInsetsCompat.Type.navigationBars()
                ).right + resources.getDimensionPixelSize(R.dimen.fab_margin)
            )

            (binding.removeKebab.layoutParams as MarginLayoutParams).updateMargins(
                bottom = insets.getInsets(
                    WindowInsetsCompat.Type.navigationBars()
                ).bottom
            )
            (binding.toggleByLast.layoutParams as MarginLayoutParams).updateMargins(
                top = insets.getInsets(
                    WindowInsetsCompat.Type.statusBars()
                ).top + resources.getDimensionPixelSize(R.dimen.period_selector_margin)
            )
            insets
        }
    }

    data class MapInstanceState(val zoom: Double, val lat: Double, val lng: Double) : java.io.Serializable

    override fun onSaveInstanceState(outState: Bundle) {
        binding.map.run {
            outState.putSerializable(
                "map",
                MapInstanceState(zoomLevelDouble, mapCenter.latitude, mapCenter.longitude)
            )
        }
        super.onSaveInstanceState(outState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.observe(state = ::render, sideEffect = ::handleSideEffect, lifecycleOwner = this)
        viewModel.observeAuthRequest(this)

        binding.fab.setOnClickListener {
            createNewPoint(binding.map.mapCenter)
        }

        binding.toggleByLast.addOnButtonCheckedListener { group, checkedId, isChecked ->
            requestPointsFromMapParams()
        }

        kebabTransition = Slide(Gravity.BOTTOM).apply {
            duration = 400
            addTarget(binding.removeKebab)
        }

        initMap(savedInstanceState)

        val currentBackStackEntry = findNavController().currentBackStackEntry!!
        currentBackStackEntry.savedStateHandle.getLiveData<PointUIModel>("point")
            .observe(currentBackStackEntry) { p ->
                populatePoint(p, true)
            }
    }

    private fun initMap(savedInstanceState: Bundle?) {
        val (zoom, lat, lng) = savedInstanceState?.getSerializable("map") as MapInstanceState? ?: MapInstanceState(
            16.5,
            55.7558,
            37.6173
        )
        binding.map.controller.apply {
            setZoom(zoom)
            setCenter(GeoPoint(lat, lng))
        }

        binding.map.addMapListener(DelayedMapListener(object : MapListener {
            override fun onScroll(event: ScrollEvent?): Boolean {
                requestPointsFromMapParams()
                return false
            }

            override fun onZoom(event: ZoomEvent?): Boolean {
                return false
            }
        }))

        binding.map.setMultiTouchControls(true)
        binding.map.zoomController.setVisibility(CustomZoomButtonsController.Visibility.NEVER)

        binding.map.overlays += MapEventsOverlay(object : MapEventsReceiver {
            override fun singleTapConfirmedHelper(p: GeoPoint): Boolean {
                return false
            }

            override fun longPressHelper(p: GeoPoint): Boolean {
                createNewPoint(p)
                return true
            }
        })
    }

    private fun showError(@StringRes strId: Int) {
        Toast.makeText(requireContext(), strId, Toast.LENGTH_LONG)
            .show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        markersMap.clear()
        _binding = null
    }

    private fun createNewPoint(p: IGeoPoint) {
        findNavController().navigate(
            MapFragmentDirections.actionMapFragmentToAddPigDialogFragment(
                Params(
                    p.latitude,
                    p.longitude
                )
            )
        )
    }

    private fun populatePoints(data: List<PointUIModel>) {
        data.forEach(::populatePoint)
    }

    private val removeActiveColor by lazy { ContextCompat.getColor(requireContext(), R.color.RemoveKebabActive) }
    private val removeInactiveColor by lazy {
        ContextCompat.getColor(
            requireContext(),
            R.color.RemoveKebabInactive
        )
    }

    private fun populatePoint(point: PointUIModel, invalidate: Boolean = false) {
        val m = markersMap.getOrPut(point.point.id) {
            HapticMarker(binding.map).apply {
                icon = ContextCompat.getDrawable(requireContext(), typeToPic(point.point.pigType))
                id = point.point.id.toString()
                isDraggable = point.isMyPoint
                setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_CENTER)
                binding.map.overlays.add(this)
                setOnMarkerDragListener(markerDragListener)
                val x = binding.map.projection.getLongPixelXFromLongitude(point.point.location.lng).toInt()
                val y = binding.map.projection.getLongPixelYFromLatitude(point.point.location.lat).toInt()
                binding.map.postInvalidateMapCoordinates(x - 10, y - 10, x + 10, y + 10)
            }
        }

        m.position = GeoPoint(point.point.location.lat, point.point.location.lng)
        m.title = point.info
        m.relatedObject = point
    }

    private fun requestPointsFromMapParams() {

        val topLeft = binding.map.projection.fromPixels(0, 0)
        val center = binding.map.mapCenter

        val distanceFromCenterToCornerOfScreen = FloatArray(1)
        android.location.Location.distanceBetween(
            center.latitude,
            center.longitude,
            topLeft.latitude,
            topLeft.longitude,
            distanceFromCenterToCornerOfScreen
        )
        val radius = distanceFromCenterToCornerOfScreen[0].roundToInt() * 3

        val byPeriod = when (binding.toggleByLast.checkedButtonId) {
            R.id.buttonHour -> ByPeriod.HOUR
            R.id.buttonDay -> ByPeriod.DAY
            R.id.buttonWeek -> ByPeriod.WEEK
            else -> ByPeriod.DAY
        }

        if (byPeriodSelection != byPeriod) {
            binding.map.overlays.removeAll { it is HapticMarker }
            markersMap.clear()
            binding.map.invalidate()
        }
        byPeriodSelection = byPeriod

        viewModel.load(center.latitude, center.longitude, radius, byPeriod)
    }

    private val markerDragListener = object : Marker.OnMarkerDragListener {
        override fun onMarkerDrag(marker: Marker) {
            binding.removeKebab.setBackgroundColor(if (marker.isReadyToRemove()) removeActiveColor else removeInactiveColor)
        }

        override fun onMarkerDragEnd(marker: Marker) {
            val point = marker.relatedObject as PointUIModel
            if (marker.isReadyToRemove()) {
                viewModel.delete(point.point.id)
                binding.map.overlays.remove(marker)
                binding.map.invalidate()
            } else {
                viewModel.update(
                    point.point.copy(location = Location(marker.position.latitude, marker.position.longitude))
                )
            }

            TransitionManager.beginDelayedTransition(binding.mapContainer, kebabTransition)
            binding.removeKebab.isVisible = false

            binding.fab.show()
        }

        override fun onMarkerDragStart(marker: Marker) {
            val point = marker.relatedObject as PointUIModel
            updateCandidateModel = Pair(point, marker)

            TransitionManager.beginDelayedTransition(binding.mapContainer, kebabTransition)
            binding.removeKebab.isVisible = true

            binding.fab.hide()
        }

        private fun Marker.isReadyToRemove(): Boolean {
            val yPos = binding.map.projection.getLongPixelYFromLatitude(position.latitude).toInt()
            return yPos > binding.removeKebab.y
        }
    }

    private fun render(state: MapState) {
        populatePoints(state.data)
    }

    private fun handleSideEffect(sideEffect: MapSideEffect) {

        when (sideEffect) {
            is MapSideEffect.AddPointError -> {
                showError(R.string.add_error)
            }
            is MapSideEffect.DefaultError -> {
                showError(R.string.something_went_wrong)
            }
            is MapSideEffect.DeletePointError -> {
                populatePoint(updateCandidateModel!!.first, true)
                showError(R.string.delete_error)
            }
            is MapSideEffect.LoadPointsError -> {
                showError(R.string.something_went_wrong)
            }
            is MapSideEffect.LoadedFromDB -> {
                showError(R.string.loaded_from_db)
            }
            is MapSideEffect.UpdatePointError -> {
                showError(R.string.update_error)

                val (p, m) = updateCandidateModel ?: return
                m.position.latitude = p.point.location.lat
                m.position.longitude = p.point.location.lng
                m.relatedObject = p
                binding.map.invalidate()
            }
        }
    }
}