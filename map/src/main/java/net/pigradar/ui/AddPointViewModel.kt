package net.pigradar.ui

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import net.pigradar.AuthStateDelegate
import net.pigradar.data.CreatePointReq
import net.pigradar.data.PigType
import net.pigradar.data.PointsRepo
import net.pigradar.data.network.ApiService
import net.pigradar.onAFailure
import net.pigradar.runACatching
import org.orbitmvi.orbit.ContainerHost
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.postSideEffect
import org.orbitmvi.orbit.syntax.simple.reduce
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject

@HiltViewModel
internal class AddPointViewModel @Inject constructor(private val pointsRepo: PointsRepo, authStateDelegate: AuthStateDelegate) :
    ViewModel(),
    AuthStateDelegate by authStateDelegate, ContainerHost<AddPointState, AddPointSideEffect> {

    override val container = container<AddPointState, AddPointSideEffect>(AddPointState.Idle)

    fun add(lat: Double, lng: Double, type: PigType, title: String, description: String?) = intent {
        reduce { AddPointState.Loading }
        runACatching {
            val p = pointsRepo.addPoint(CreatePointReq(lat, lng, title, type, description))
            reduce {
                AddPointState.Success(p.toUiModel(true))
            }
        }.onAFailure {
            postSideEffect(AddPointSideEffect.Error(it))
            reduce { AddPointState.Idle }
        }
    }
}

internal sealed class AddPointState {
    object Idle : AddPointState()
    object Loading : AddPointState()
    data class Success(val point: PointUIModel) : AddPointState()
}

internal sealed class AddPointSideEffect {
    data class Error(val e: Throwable) : AddPointSideEffect()
}