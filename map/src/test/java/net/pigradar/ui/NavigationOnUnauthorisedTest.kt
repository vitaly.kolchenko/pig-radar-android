package net.pigradar.navigation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelStore
import androidx.navigation.NavDestination
import androidx.navigation.NavDestinationBuilder
import androidx.navigation.Navigation
import androidx.navigation.createGraph
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltTestApplication
import dagger.hilt.android.testing.UninstallModules
import dagger.hilt.components.SingletonComponent
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.util.*
import io.ktor.util.date.*
import io.ktor.utils.io.*
import io.ktor.utils.io.errors.*
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import net.pigradar.LOGIN_DL
import net.pigradar.PigConfig
import net.pigradar.data.PointsRepo
import net.pigradar.di.RepoModule
import net.pigradar.ui.AddPointDialogFragment
import net.pigradar.ui.AddPointSideEffect
import net.pigradar.ui.AddPointState
import net.pigradar.ui.AddPointViewModel
import net.pigradar.ui.ByPeriod
import net.pigradar.ui.MapFragment
import net.pigradar.ui.MapSideEffect
import net.pigradar.ui.MapState
import net.pigradar.ui.MapViewModel
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.orbitmvi.orbit.ContainerHost
import org.orbitmvi.orbit.liveTest
import org.robolectric.annotation.Config
import kotlin.reflect.full.createInstance
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.isAccessible

@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
@Config(application = HiltTestApplication::class)
@UninstallModules(RepoModule::class)
class NavigationOnUnauthorisedTest {

    @Module
    @InstallIn(SingletonComponent::class)
    internal class FakeModule {

        @Provides
        fun pigConfig(): PigConfig = PigConfig("test")

        @Provides
        fun dispatcher(): CoroutineDispatcher = Dispatchers.Main

        @Provides
        fun providesRepo(
        ): PointsRepo = mockk {
            coEvery { addPoint(any()) } throws mockk<ResponseException> {
                every { response.status } returns HttpStatusCode.Unauthorized
            }

            coEvery { getPoints(any(), any(), any(), any()) } throws mockk<ResponseException> {
                every { response.status } returns HttpStatusCode.Unauthorized
            }
        }
    }

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Test
    fun `unauthorized response NAVIGATES to login (ADD POINT)`() {
        testOrbitFragment<AddPointDialogFragment, AddPointViewModel, AddPointState, AddPointSideEffect> {
            add(0.0, 12.0, net.pigradar.data.PigType.COP_PIG, "", null)
        }
    }

    @Test
    fun `unauthorized response NAVIGATES to login (MAP)`() {
        testOrbitFragment<MapFragment, MapViewModel, MapState, MapSideEffect> {
            load(0.0, 12.0, 2, ByPeriod.DAY)
        }
    }
}

object TestRoutes {
    const val content = "content"
    const val login = "login"
}

/**
 * test checks that unauthorized response causes navigation to login screen.
 *
 * [block] - unauthorized call here
 */
@OptIn(ExperimentalCoroutinesApi::class)
inline fun <reified F : Fragment, reified VM : ContainerHost<S, SE>, S : Any, SE : Any> testOrbitFragment(
    vmFieldName: String = "viewModel",
    crossinline block: VM.() -> Unit
) {
    val navController = TestNavHostController(
        ApplicationProvider.getApplicationContext()
    )
    navController.setViewModelStore(ViewModelStore())

    navController.graph = buildTestNavGraph(navController)
    val f = F::class.createInstance()

    launchFragmentInContainer {
        f.also { fragment ->

            // In addition to returning a new instance of our Fragment,
            // get a callback whenever the fragment’s view is created
            // or destroyed so that we can set the NavController
            fragment.viewLifecycleOwnerLiveData.observeForever { viewLifecycleOwner ->
                if (viewLifecycleOwner != null) {

                    // The fragment’s view has just been created

                    Navigation.setViewNavController(fragment.requireView(), navController)

                    val vmProp = F::class.memberProperties.find { it.name == vmFieldName }
                    val vm = vmProp!!.let {
                        it.isAccessible = true
                        it.get(fragment) as VM
                    }

                    val testContainer = vm.liveTest {
                        dispatcher = UnconfinedTestDispatcher()
                    }
                    testContainer.testIntent {
                        block()
                    }
                }
            }
        }
    }
    assertEquals(TestRoutes.login, navController.currentDestination?.route)
}

fun buildTestNavGraph(navController: TestNavHostController) = navController.createGraph(
    startDestination = TestRoutes.content
) {
    destination(
        NavDestinationBuilder(
            navController.navigatorProvider.getNavigator("test"),
            TestRoutes.content
        )
    )

    destination(NavDestinationBuilder<NavDestination>(
        navController.navigatorProvider.getNavigator("test"),
        TestRoutes.login
    ).apply { deepLink(LOGIN_DL) })
}


