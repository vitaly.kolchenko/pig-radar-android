package net.pigradar.ui

import net.pigradar.data.Location
import net.pigradar.data.PigType
import net.pigradar.data.Point
import org.junit.After
import org.junit.Before
import org.mockito.MockitoAnnotations
import java.util.*


abstract class MockitoTestCase {
    private var closeable: AutoCloseable? = null

    @Before
    fun openMocks() {
        closeable = MockitoAnnotations.openMocks(this)
    }

    @After
    @Throws(java.lang.Exception::class)
    fun releaseMocks() {
        closeable!!.close()
    }
}

internal fun pointGen(id: Int = 1) = Point(id, "test", PigType.COP_PIG, null, 0, 0, 1, Location(14.0, 24.2), Date())
internal fun pointUiGen(id: Int = 1) =
    pointGen(id).toUiModel(true)