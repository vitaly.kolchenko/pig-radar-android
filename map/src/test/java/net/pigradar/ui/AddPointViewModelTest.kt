package net.pigradar.ui

import io.ktor.utils.io.errors.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import net.pigradar.AuthStateDelegate
import net.pigradar.AuthStateDelegateImpl
import net.pigradar.data.Location
import net.pigradar.data.PigType
import net.pigradar.data.Point
import net.pigradar.data.PointsRepo
import net.pigradar.data.Result
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.any
import org.mockito.kotlin.whenever
import org.orbitmvi.orbit.test
import java.util.*


@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(MockitoJUnitRunner::class)
internal class AddPointViewModelTest : MockitoTestCase() {

    @Mock
    private lateinit var pointsRepo: PointsRepo

    private val auth: AuthStateDelegate = AuthStateDelegateImpl()

    private lateinit var vm: AddPointViewModel

    @Before
    fun init() {
        vm = AddPointViewModel(pointsRepo, auth)
    }

    @Test
    fun `add point RETURNS state idle-loading-success`() = runTest {

        //Given
        val p = pointGen()
        whenever(
            pointsRepo.addPoint(any())
        ).thenReturn(p)

        //When
        val testSubject = vm.test(AddPointState.Idle)
        testSubject.testIntent {
            add(
                p.location.lat,
                p.location.lng,
                p.pigType,
                p.title,
                p.comment
            )
        }

        //Then
        testSubject.assert(AddPointState.Idle) {
            postedSideEffects()

            states(
                { AddPointState.Idle },
                { AddPointState.Loading },
                { AddPointState.Success(p.toUiModel(true)) })
        }
    }

    @Test
    fun `add point with exception RETURNS side effect and idle state`() = runTest {

        //Given
        val e = RuntimeException("test")
        val p = pointGen()
        whenever(
            pointsRepo.addPoint(any())
        ).thenThrow(e)

        //When
        val testSubject = vm.test(AddPointState.Idle)
        testSubject.testIntent {
            add(
                p.location.lat,
                p.location.lng,
                p.pigType,
                p.title,
                p.comment
            )
        }

        //Then
        testSubject.assert(AddPointState.Idle) {
            postedSideEffects(AddPointSideEffect.Error(e))

            states(
                { AddPointState.Idle },
                { AddPointState.Loading },
                { AddPointState.Idle })
        }
    }
}


