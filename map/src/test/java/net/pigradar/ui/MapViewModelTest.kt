package net.pigradar.ui

import io.ktor.utils.io.errors.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import net.pigradar.AuthStateDelegate
import net.pigradar.AuthStateDelegateImpl
import net.pigradar.data.PointsRepo
import net.pigradar.data.Result
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.any
import org.mockito.kotlin.whenever
import org.orbitmvi.orbit.test
import java.util.*


@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(MockitoJUnitRunner::class)
internal class MapViewModelTest : MockitoTestCase() {

    @Mock
    private lateinit var pointsRepo: PointsRepo

    private val auth: AuthStateDelegate = AuthStateDelegateImpl()

    private lateinit var vm: MapViewModel

    @Before
    fun init() {
        vm = MapViewModel(pointsRepo, auth)
    }

    @Test
    fun `load from network RETURNS state without side effects`() = runTest {

        //Given
        val p = pointUiGen()
        whenever(pointsRepo.getPoints(any(), any(), any(), any())).thenReturn(
            Result(
                Result.Source.NETWORK,
                listOf(p)
            )
        )

        //When
        val testSubject = vm.test()
        testSubject.testIntent {
            load(0.0, 1.0, 4000, ByPeriod.DAY)
        }

        //Then
        testSubject.assert(MapState(emptyList())) {
            postedSideEffects()

            states({
                copy(data + p)
            })
        }
    }

    @Test
    fun `load with exception RETURNS no state and side effects`() = runTest {

        //Given
        val p = pointUiGen()
        val e = RuntimeException("test")
        whenever(pointsRepo.getPoints(any(), any(), any(), any())).thenThrow(e)

        //When
        val testSubject = vm.test()
        testSubject.testIntent {
            load(0.0, 1.0, 4000, ByPeriod.DAY)
        }

        //Then
        testSubject.assert(MapState(emptyList())) {
            postedSideEffects(MapSideEffect.LoadPointsError(e))

            states()
        }
    }

    @Test
    fun `load from db RETURNS state with side effect`() = runTest {
        //Given
        val p = pointUiGen()
        whenever(pointsRepo.getPoints(any(), any(), any(), any())).thenReturn(
            Result(
                Result.Source.DB,
                listOf(p)
            )
        )

        //When
        val testSubject = vm.test()
        testSubject.testIntent {
            load(0.0, 1.0, 4000, ByPeriod.DAY)
        }

        //Then
        testSubject.assert(MapState(emptyList())) {
            postedSideEffects(MapSideEffect.LoadedFromDB)

            states({
                copy(data + p)
            })
        }
    }

    @Test
    fun `update point UPDATES state and repo`() = runTest {

        //Given
        val p1 = pointUiGen(1)
        val p2 = pointUiGen(2)
        val p3 = pointUiGen(3)
        val updatedP2 = p2.copy(point = p2.point.copy(title = "Updated"))

        //When
        val initialState = MapState(listOf(p1, p2, p3))
        val testSubject = vm.test(initialState)
        testSubject.testIntent {
            update(updatedP2.point)
        }

        //Then
        testSubject.assert(initialState) {
            postedSideEffects()

            states({
                copy(listOf(p1, updatedP2, p3))
            })
        }
    }

    @Test
    fun `update point error RETURNS side effect`() = runTest {

        //Given
        val p1 = pointUiGen(1)
        val e = RuntimeException("test")
        whenever(pointsRepo.updatePoint(any())).thenThrow(e)

        //When
        val initialState = MapState(listOf(p1))
        val testSubject = vm.test(initialState)
        testSubject.testIntent {
            update(p1.point)
        }

        //Then
        testSubject.assert(initialState) {
            postedSideEffects(MapSideEffect.UpdatePointError(e))

            states()
        }
    }

    @Test
    fun `delete point UPDATES state and repo`() = runTest {

        //Given
        val p1 = pointUiGen(1)
        val p2 = pointUiGen(2)
        val p3 = pointUiGen(3)

        //When
        val initialState = MapState(listOf(p1, p2, p3))
        val testSubject = vm.test(initialState)
        testSubject.testIntent {
            delete(p2.point.id)
        }

        //Then
        testSubject.assert(initialState) {
            postedSideEffects()

            states({
                copy(listOf(p1, p3))
            })
        }
    }

    @Test
    fun `delete point error RETURNS side effect`() = runTest {

        //Given
        val p1 = pointUiGen(1)
        val e = RuntimeException("test")
        whenever(pointsRepo.deletePoint(any())).thenThrow(e)

        //When
        val initialState = MapState(listOf(p1))
        val testSubject = vm.test(initialState)
        testSubject.testIntent {
            delete(p1.point.id)
        }

        //Then
        testSubject.assert(initialState) {
            postedSideEffects(MapSideEffect.DeletePointError(e))

            states()
        }
    }


}


