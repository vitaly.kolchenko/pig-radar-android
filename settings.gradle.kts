enableFeaturePreview("VERSION_CATALOGS")
enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")


include(":app")
include(":auth")
include(":pig-data")
include(":map")
include(":core")

pluginManagement {

    repositories {
        google()
        mavenCentral()
        maven("https://plugins.gradle.org/m2/")//for kapt plugin 1.5.30
    }
}

dependencyResolutionManagement {

    repositories {
        google()
        mavenCentral()
    }
}