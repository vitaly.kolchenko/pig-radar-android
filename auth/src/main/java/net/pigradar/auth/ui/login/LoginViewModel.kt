package net.pigradar.auth.ui.login

import android.util.Patterns
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import net.pigradar.auth.R
import net.pigradar.auth.data.LoginRepository
import org.orbitmvi.orbit.ContainerHost
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.syntax.simple.postSideEffect
import org.orbitmvi.orbit.syntax.simple.reduce
import org.orbitmvi.orbit.viewmodel.container
import javax.inject.Inject

@HiltViewModel
internal class LoginViewModel @Inject constructor(private val loginRepository: LoginRepository) : ViewModel(),
    ContainerHost<LoginFormState, LoginSideEffect> {

    override val container = container<LoginFormState, LoginSideEffect>(LoginFormState(isDataValid = true))

    fun login(username: String, password: String) = intent {
        // can be launched in a separate asynchronous job
        runCatching {
            loginRepository.login(username, password)
            reduce { LoginFormState(successfulLogin = true) }
        }.onFailure {
            postSideEffect(LoginSideEffect.Error(it))
        }
    }

    fun loginDataChanged(username: String, password: String) = intent {
        reduce {
            when {
                !isUserNameValid(username) -> {
                    LoginFormState(usernameError = R.string.invalid_username)
                }
                !isPasswordValid(password) -> {
                    LoginFormState(passwordError = R.string.invalid_password)
                }
                else -> {
                    LoginFormState(isDataValid = true)
                }
            }
        }

    }

    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return if (username.contains("@")) {
            Patterns.EMAIL_ADDRESS.matcher(username).matches()
        } else {
            username.isNotBlank()
        }
    }

    // A placeholder password validation check
    private fun isPasswordValid(password: String): Boolean {
        return password.length > 4
    }
}

data class LoginFormState(
    val usernameError: Int? = null,
    val passwordError: Int? = null,
    val isDataValid: Boolean = false,
    val successfulLogin: Boolean = false
)

sealed class LoginSideEffect{
   class Error(e: Throwable): LoginSideEffect()
}