package net.pigradar.auth.data.network

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import net.pigradar.auth.BuildConfig
import net.pigradar.auth.data.model.LoginRequest
import net.pigradar.auth.data.model.LoginResponse
import javax.inject.Inject

private val BASE_URL = "http://${BuildConfig.baseUrl}:8081"
private val LOGIN = "$BASE_URL/login"

internal class ApiServiceImpl @Inject constructor(private val httpClient: HttpClient) : ApiService {

    override suspend fun login(login: String, password: String): LoginResponse {
        return httpClient.post(LOGIN) { setBody(LoginRequest(login, password)) }.body()
    }
}