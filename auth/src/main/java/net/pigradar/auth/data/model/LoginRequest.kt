package net.pigradar.auth.data.model

@kotlinx.serialization.Serializable
internal data class LoginRequest(val login: String, val password: String)