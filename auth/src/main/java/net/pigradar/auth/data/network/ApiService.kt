package net.pigradar.auth.data.network

import net.pigradar.auth.data.model.LoginResponse

internal interface ApiService {
    suspend fun login(login: String, password: String): LoginResponse
}