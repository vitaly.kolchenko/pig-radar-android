package net.pigradar.auth.data.model

@kotlinx.serialization.Serializable
internal data class LoginResponse(val token: String)