package net.pigradar.auth.data

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import net.pigradar.LocalStore
import net.pigradar.auth.data.network.ApiService
import net.pigradar.token
import javax.inject.Inject

internal class LoginRepository @Inject constructor(
    private val apiService: ApiService,
    private val localStore: LocalStore
) {

    suspend fun login(username: String, password: String): Unit {
        return withContext(Dispatchers.IO) {
            val response = apiService.login(username, password)
            localStore.token(response.token)
        }
    }
}