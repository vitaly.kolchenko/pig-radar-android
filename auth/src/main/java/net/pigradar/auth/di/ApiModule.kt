package net.pigradar.auth.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import net.pigradar.auth.data.network.ApiService
import net.pigradar.auth.data.network.ApiServiceImpl

@Module
@InstallIn(SingletonComponent::class)
internal abstract class ApiModule {
    @Binds
    abstract fun bindApi(
        apiServiceImpl: ApiServiceImpl
    ): ApiService
}