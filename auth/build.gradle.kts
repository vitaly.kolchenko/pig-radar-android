plugins {
    id("android-library-convention")
    id("kotlin-kapt-convention")
}

android {
    defaultConfig {
        namespace = "net.pigradar.auth"
    }
    buildFeatures {
        viewBinding = true
        dataBinding = true
    }
}



dependencies {
    implementation(projects.core)

    implementation(libs.bundles.ktor)

    implementation(libs.bundles.androidx)

    testImplementation(libs.test.junit)
    androidTestImplementation(libs.androidx.test.junit.ext)
    androidTestImplementation(libs.androidx.test.espresso)
}
