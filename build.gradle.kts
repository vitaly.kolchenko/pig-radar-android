import com.android.build.gradle.BaseExtension

plugins {
    alias(libs.plugins.hilt) apply false
}

buildscript {
    dependencies {
        classpath(libs.kotlinSerialization)
        classpath(libs.androidx.navPlugin)
    }
}

val baseUrl = if (project.hasProperty("baseUrl")) "\"${project.property("baseUrl")}\"" else "\"192.168.0.128\""

allprojects {
    val minSdkVersion by extra(21)
    val compileSdkVersion by extra(32)

    afterEvaluate {
        (extensions.findByName("android") as? BaseExtension)?.apply {
            defaultConfig {
                buildConfigField("String", "baseUrl", baseUrl)
            }
        }
    }
}

task<Delete>("clean") {
    delete(rootProject.buildDir)
}
