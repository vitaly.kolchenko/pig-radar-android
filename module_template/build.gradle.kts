plugins {
    id("android-library-convention")
    id("kotlin-kapt-convention")
}

android {
    defaultConfig {
        namespace = "net.pigradar.template"
    }
    buildFeatures {
        //viewBinding = true
    }
}



dependencies {
    testImplementation(libs.test.junit)
    androidTestImplementation(libs.androidx.test.junit.ext)
    androidTestImplementation(libs.androidx.test.espresso)
}
