import com.android.build.gradle.BaseExtension
import org.gradle.accessors.dm.LibrariesForLibs

plugins {
    id("kotlin-kapt")
}

project.withVersionCatalog { libs ->
    plugins {
        alias(libs.plugins.hilt)
    }

    dependencies {
        //add(Configurations.kapt, platform(libs.kotlinBom))

        add("kapt", libs.hiltCompiler)
        add("implementation", libs.hilt)
    }
}

configure<BaseExtension> {
// Allow references to generated code
    kapt {
        correctErrorTypes = true
    }
}

/**
 * workaround to make version catalog accessible in convention plugins
 * https://github.com/gradle/gradle/issues/15383
 */
fun Project.withVersionCatalog(block: (libs: LibrariesForLibs) -> Unit) {
    val libs = the<org.gradle.accessors.dm.LibrariesForLibs>()
    block.invoke(libs)
}