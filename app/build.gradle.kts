plugins {
    id("android-application-convention")
    id("kotlin-kapt-convention")
}

android {
    defaultConfig {
        applicationId = "net.pigradar"
        namespace = "net.pigradar"
    }

    buildFeatures {
        dataBinding = true
        viewBinding = true
    }
}



dependencies {
    implementation(projects.core)

    implementation(projects.auth)

    implementation(projects.map)

    implementation(libs.androidx.room.runtime)
    implementation(libs.androidx.room.ktx)
    kapt(libs.androidx.room.compiler)

    implementation(libs.androidx.navigation.fragment)
    implementation(libs.androidx.navigation.ui)
    implementation(libs.material)

    testImplementation(libs.bundles.unitTest)

    androidTestImplementation(libs.androidx.test.junit.ext)
    androidTestImplementation(libs.androidx.test.espresso)
}
