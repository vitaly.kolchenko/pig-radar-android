package net.pigradar.di

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import net.pigradar.data.db.LPConverters
import net.pigradar.data.db.LocalPoint
import net.pigradar.data.db.LocalPointDao

@Database(entities = [LocalPoint::class], version = 1)
@TypeConverters(LPConverters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): LocalPointDao
}