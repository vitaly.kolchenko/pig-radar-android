package net.pigradar.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import net.pigradar.BuildConfig
import net.pigradar.PigConfig
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ConfigModule {

    @Provides
    @Singleton
    fun provideConfig() = PigConfig(BuildConfig.APPLICATION_ID)
}