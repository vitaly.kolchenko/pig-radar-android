package net.pigradar.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import net.pigradar.AuthStateDelegate
import net.pigradar.AuthStateDelegateImpl

@Module
@InstallIn(SingletonComponent::class)
abstract class AuthDelegateModule {
    @Binds
    abstract fun bindApi(
        authDelegateImpl: AuthStateDelegateImpl
    ): AuthStateDelegate
}