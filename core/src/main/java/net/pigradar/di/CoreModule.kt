package net.pigradar.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import net.pigradar.LocalStore
import net.pigradar.LocalStoreImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class CoreModule {

    @Binds
    @Singleton
    internal abstract fun bindStore(l: LocalStoreImpl): LocalStore
}