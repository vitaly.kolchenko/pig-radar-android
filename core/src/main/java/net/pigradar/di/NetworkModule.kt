package net.pigradar.di

import android.util.Log
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.ktor.client.*
import io.ktor.client.engine.android.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.auth.*
import io.ktor.client.plugins.auth.providers.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.kotlinx.serializer.*
import io.ktor.client.plugins.logging.*
import io.ktor.client.plugins.observer.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import net.pigradar.LocalStore
import net.pigradar.token
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class NetworkModule {

    companion object {
        @Provides
        @Singleton
        fun ktorHttpClient(localStore: LocalStore) = HttpClient(Android) {

            System.setProperty("kotlinx.coroutines.debug", "on")

            expectSuccess = true

            install(ContentNegotiation) {
                json()

                engine {
                    connectTimeout = 15_000
                    socketTimeout = 15_000
                }
            }

            install(Logging) {
                logger = object : Logger {
                    override fun log(message: String) {
                        Log.v("Logger Ktor =>", "${Thread.currentThread().name} $message")
                    }

                }
                level = LogLevel.ALL
            }

            install(ResponseObserver) {
                onResponse { response ->
                    Log.d("HTTP status:", "${response.status.value}")
                }
            }

            install(DefaultRequest) {
                bearerAuth(localStore.token() ?: "")
                header(HttpHeaders.ContentType, ContentType.Application.Json)
            }
        }
    }
}