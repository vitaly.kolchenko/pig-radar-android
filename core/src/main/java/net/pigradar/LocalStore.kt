package net.pigradar

import com.auth0.android.jwt.JWT

interface LocalStore {
    operator fun get(key: String): String?

    operator fun set(key: String, value: String)
}

fun LocalStore.token() = this["token"]

fun LocalStore.token(value: String): Unit {
    this["token"] = value
}

fun LocalStore.userId() = token()?.let { JWT(it).getClaim("id").asInt() }
