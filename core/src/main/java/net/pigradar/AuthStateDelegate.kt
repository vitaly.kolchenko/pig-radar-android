package net.pigradar

import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.fragment.findNavController
import io.ktor.client.plugins.*
import io.ktor.http.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

interface AuthStateDelegate {
    val authFlow: Flow<AuthRequest>
    fun requestAuthIfNeeded(e: Throwable): Boolean
}

enum class AuthRequest {
    NEED_AUTH
}

class AuthStateDelegateImpl @Inject constructor() : AuthStateDelegate {
    private val authChannel = Channel<AuthRequest>(Channel.BUFFERED)
    override val authFlow = authChannel.receiveAsFlow()

    override fun requestAuthIfNeeded(e: Throwable): Boolean {
        val isAuthException = e.isAuthException()
        if (isAuthException) {
            val result = authChannel.trySend(AuthRequest.NEED_AUTH)
            result.exceptionOrNull()?.let { Timber.e(it) }
        }

        return isAuthException
    }
}

fun AuthStateDelegate.observeAuthRequest(fragment: Fragment) {
    fragment.lifecycleScope.launch {
        fragment.lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
            authFlow.collect {
                fragment.findNavController().navigate(goToLogin())
            }
        }
    }
}

fun Throwable.isAuthException() = this is ResponseException && this.response.status == HttpStatusCode.Unauthorized

const val LOGIN_DL = "android-app://net.pigradar.app/login"
fun goToLogin() = NavDeepLinkRequest.Builder.fromUri(LOGIN_DL.toUri()).build()

inline fun <R> AuthStateDelegate.runACatching(block: () -> R): Result<R> {
    val result = runCatching(block)
    if (result.isFailure) {
        requestAuthIfNeeded(result.exceptionOrNull()!!)
    }
    return result
}

inline fun <T> Result<T>.onAFailure(action: (exception: Throwable) -> Unit): Result<T> {
    val e = exceptionOrNull()
    if (e?.isAuthException() == false) {
        Timber.e(e)
        action(e)
    }
    return this
}