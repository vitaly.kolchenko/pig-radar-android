package net.pigradar

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

internal class LocalStoreImpl @Inject constructor(@ApplicationContext private val context: Context) : LocalStore {
    private val prefs = context.getSharedPreferences("LocalStore", Context.MODE_PRIVATE)

    override fun get(key: String): String? {
        return prefs.getString(key, null)
    }

    override fun set(key: String, value: String) {
        prefs.edit().putString(key, value).apply()
    }
}