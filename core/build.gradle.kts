plugins {
    id("android-library-convention")
    id("kotlin-kapt-convention")
}

android {
    defaultConfig {
        namespace = "net.pigradar.core"
    }
    buildFeatures {
        //viewBinding = true
    }
}



dependencies {
    api(libs.bundles.ktor)
    api(libs.timber)
    api(libs.mviorbit.viewmodel)
    implementation(libs.material)
    implementation(libs.androidx.navigation.fragment)
    testImplementation(libs.test.junit)
    androidTestImplementation(libs.androidx.test.junit.ext)
    androidTestImplementation(libs.androidx.test.espresso)
}
